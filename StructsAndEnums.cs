﻿using System;

namespace VKMonoBinding {
	public enum VKProgressType {
		VKProgressTypeUpload, VKProgressTypeDownload
	}

	public enum VKImageType {
		VKImageTypeJpg, VKImageTypePng
	}
}

