﻿using System;
using System.Drawing;
using MonoTouch.ObjCRuntime;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace VKMonoBinding {
	delegate void ProgressCallback (VKProgressType progressType, long bytesLoaded, long BytesTotal);
	delegate void CompleteCallback (VKResponse response);
	delegate void ErrorCallback (NSError error);

	#region API
	[BaseType (typeof(NSObject))]
	interface VKObject { }

	[BaseType (typeof(VKObject))]
	interface VKApiBase {
		[Export ("getMethodGroup")]
		string GetMethodGroup ();

		[Export ("prepareRequestWithMethodName:andParameters:")]
		VKRequest PrepareRequest (string methodName, NSDictionary methodParameters);

		[Export ("prepareRequestWithMethodName:andParameters:andHttpMethod:")]
		VKRequest PrepareRequest (string methodName, NSDictionary methodParameters, string httpMethod);

		[Export ("prepareRequestWithMethodName:andParameters:andHttpMethod:andClassOfModel:")]
		VKRequest PrepareRequest (string methodName, NSDictionary methodParameters, string httpMethod, Class modelClass);
	}

	[BaseType (typeof(VKApiBase))]
	interface VKApiUsers {
		[Export ("get")]
		VKRequest Get ();

		[Export ("get:")]
		VKRequest Get (NSDictionary getParams);

		[Export ("search:")]
		VKRequest Search (NSDictionary getParams);

		[Export ("isAppUser")]
		VKRequest IsAppUser ();

		[Export ("isAppUser:")]
		VKRequest IsAppUser (int userId);

		[Export ("getSubscriptions")]
		VKRequest GetSubscriptions ();

		[Export ("getSubscriptions:")]
		VKRequest GetSubscriptions (NSDictionary getParams);

		[Export ("getFollowers")]
		VKRequest GetFollowers ();

		[Export ("getFollowers:")]
		VKRequest GetFollowers (NSDictionary getParams);
	}

	[BaseType (typeof(VKApiBase))]
	interface VKApiWall {
		[Export ("post:")]
		VKRequest Post (NSDictionary getParams);
	}

	[BaseType (typeof(VKApiBase))]
	interface VKApiPhotos {
		[Export ("getUploadServer:")]
		VKRequest GetUploadServer (int albumId);

		[Export ("getUploadServer:andGroupId:")]
		VKRequest GetUploadServer (int albumId, int groupId);

		[Export ("getWallUploadServer")]
		VKRequest GetWallUploadServer ();

		[Export ("getWallUploadServer:")]
		VKRequest GetWallUploadServer (int groupId);

		[Export ("save:")]
		VKRequest Save (NSDictionary getParams);

		[Export ("saveWallPhoto:")]
		VKRequest SaveWallPhoto (NSDictionary getParams);
	}

	[BaseType (typeof(VKApiBase))]
	interface VKApiFriends {
		[Export ("get")]
		VKRequest Get ();

		[Export ("get:")]
		VKRequest Get (NSDictionary getParams);
	}

	[BaseType (typeof(VKObject))]
	interface VKImageParameters {
		[Export("imageType", ArgumentSemantic.Assign)]
		VKImageType ImageType { get; set; }

		[Export("jpegQuality", ArgumentSemantic.Assign)]
		float JpegQuality { get; set; }

		[Static, Export("pngImage")]
		VKImageParameters PngImage { get; }

		[Static, Export("jpegImageWithQuality:")]
		VKImageParameters JpegImageWithQuality(float quality);

		[Export ("fileExtension")]
		string FileExtension { get; }

		[Export ("mimeType")]
		string MimeType { get; }
	}

	[BaseType (typeof(VKObject))]
	interface VKUploadImage {
		[Export("imageData")]
		NSData ImageData { get; set; }

		[Export("parameters")]
		VKImageParameters Parameters { get; set; }

		[Static, Export("objectWithData:andParams:")]
		VKUploadImage WithDataAndParams (NSData data, VKImageParameters parameters);
	}
		
	[BaseType (typeof(VKObject))]
	interface VKAccessToken {
		[Export ("accessToken")]
		string AccessToken { get; set; }

		[Export ("expiresIn")]
		string ExpiresIn { get; set; }

		[Export ("userId")]
		string UserId { get; set; }

		[Export ("secret")]
		string Secret { get; set; }

		[Export ("httpsRequired", ArgumentSemantic.Assign)]
		bool HttpsRequired { get; set; }

		[Export ("created")]
		double Created { get; }

		[Export ("isExpired")]
		bool IsExpired { get; }

		[Static, Export ("tokenFromUrlString:")]
		VKAccessToken FromUrlString (string urlString);

		[Static, Export ("tokenFromFile:")]
		VKAccessToken FromFile (string filePath);

		[Static, Export ("tokenFromDefaults:")]
		VKAccessToken FromDefaults (string defaultsKey);

		[Export ("saveTokenToFile:")]
		void SaveTokenToFile (string filePath);

		[Export ("saveTokenToDefaults:")]
		void SaveTokenToDefaults (string defaultsKey);
	}

	[BaseType (typeof(VKObject))]
	interface VKRequestTiming {
		[Export ("startTime")]
		NSDate StartTime { get; set; }

		[Export ("finishTime")]
		NSDate FinishTime { get; set; }

		[Export ("loadTime")]
		double LoadTime { get; set; }

		[Export ("parseTime")]
		double parseTime { get; set; }

		[Export ("totalTime")]
		double totalTime { get; set; }
	}

	[BaseType (typeof(VKObject))]
	interface VKResponse {
		[Export ("request")]
		VKRequest Request { get; }

		[Export ("json")]
		NSObject Json { get; }

		[Export ("parsedModel")]
		NSObject ParseModel { get; }
	}

	[BaseType (typeof(VKObject))]
	interface VKRequest	{
		[Export ("preferredLang")]
		string PreferredLang { get; set; }

		[Export ("setProgressBlock:", ArgumentSemantic.Copy)]
		void SetProgressBlock (ProgressCallback progressBlock);

		[Export ("setCompleteBlock:", ArgumentSemantic.Copy)]
		void SetCompleteBlock (CompleteCallback completeBlock);

		[Export ("setErrorBlock:", ArgumentSemantic.Copy)]
		void SetErrorBlock (ErrorCallback errorBlock);

		[Export ("attempts", ArgumentSemantic.Assign)]
		int Attempts { get; set; }

		[Export ("secure", ArgumentSemantic.Assign)]
		bool Secure { get; set; }

		[Export ("useSystemLanguage", ArgumentSemantic.Assign)]
		bool UseSystemLanguage { get; set; }

		[Export ("parseModel", ArgumentSemantic.Assign)]
		bool ParseModel { get; set; }

		[Export ("debugTiming", ArgumentSemantic.Assign)]
		bool DebugTiming { get; set; }

		[Export ("requestTimeout", ArgumentSemantic.Assign)]
		int RequestTimeout { get; set; }

		[Export ("methodName")]
		string MethodName { get; }

		[Export ("httpMethod")]
		string HttpMethod { get; }

		[Export ("methodParameters")]
		NSDictionary MethodParameters { get; }

		[Export ("executionOperation")]
		NSOperation ExecutionOperation { get; }

		[Export ("requestTiming")]
		VKRequestTiming RequestTiming { get; }

		[Static, Export ("requestWithMethod:andParameters:andHttpMethod:")]
		VKRequest WithMethod (string method, NSDictionary parameters, string httpMethod);

		[Static, Export ("requestWithMethod:andParameters:andHttpMethod:classOfModel:")]
		VKRequest WithMethod (string method, NSDictionary parameters, string httpMethod, Class modelClass);

		[Static, Export ("photoRequestWithPostUrl:withPhotos:")]
		VKRequest WithMethod (string url, NSArray photoObjects);

		[Export ("getPreparedRequest")]
		NSUrlRequest GetPreparedRequest ();

		[Export ("executeWithResultBlock:errorBlock:")]
		void Execute (CompleteCallback onComplete, ErrorCallback onError);

		[Export ("executeAfter:withResultBlock:errorBlock:")]
		void ExecuteAfter (VKRequest request, CompleteCallback onComplete, ErrorCallback onError);

		[Export ("start")]
		void Start ();

		[Export ("repeat")]
		void Repeat ();

		[Export ("cancel")]
		void Cancel ();

		[Export ("addExtraParameters:")]
		void AddExtraParameters (NSDictionary parameters);
	}

	[BaseType (typeof(VKObject))]
	interface VKError {
		[Export ("httpError")]
		NSError HttpError { get; set; }

		[Export ("apiError")]
		VKError ApiError { get; set; }

		[Export ("request")]
		VKRequest Request { get; set; }

		[Export ("errorCode", ArgumentSemantic.Assign)]
		int ErrorCode { get; set; }

		[Export ("errorMessage")]
		string ErrorMessage { get; set; }

		[Export ("errorReason")]
		string ErrorReason { get; set; }

		[Export ("requestParams")]
		NSDictionary RequestParams { get; set; }

		[Export ("captchaSid")]
		string CaptchaSid { get; set; }

		[Export ("captchaImg")]
		string CaptchaImg { get; set; }

		[Export ("redirectUri")]
		string RedirectUri { get; set; }

		[Export ("answerCaptcha:")]
		void AnswerCaptcha (string userEnteredCode);

		[Static, Export ("errorWithCode:")]
		VKError WithCode (int code);

		[Static, Export ("errorWithJson:")]
		VKError WithJson (NSObject json);

		[Static, Export ("errorWithQuery:")]
		VKError WithQuery (NSDictionary queryParams);
	}

	[Model, BaseType (typeof(NSObject))]
	interface VKSdkDelegate {
		[Abstract, Export ("vkSdkNeedCaptchaEnter:")]
		void VkSdkNeedCaptchaEnter (VKError captchaError);

		[Abstract, Export ("vkSdkTokenHasExpired:")]
		void VkSdkTokenHasExpired (VKAccessToken expiredToken);

		[Abstract, Export ("vkSdkUserDeniedAccess:")]
		void VkSdkUserDeniedAccess (VKError authorizationError);

		[Abstract, Export ("vkSdkShouldPresentViewController:")]
		void VkSdkShouldPresentViewController (UIViewController controller);

		[Abstract, Export ("vkSdkReceivedNewToken:")]
		void VkSdkReceivedNewToken (VKAccessToken newToken);

		[Export ("vkSdkAcceptedUserToken:")]
		void VkSdkAcceptedUserToken (VKAccessToken token);

		[Export ("vkSdkRenewedToken:")]
		void VkSdkRenewedToken (VKAccessToken token);
	}

	[BaseType (typeof(NSObject), Delegates = new string[]{ "WeakDelegate" })]
	interface VKSdk {
		[Export ("delegate", ArgumentSemantic.Assign)]
		[NullAllowed]
		NSObject WeakDelegate { get; set; }

		[Wrap ("WeakDelegate")]
		[NullAllowed]
		VKSdkDelegate Delegate { get; set; }

		[Static, Export ("instance")]
		VKSdk Instance ();

		[Static, Export ("initializeWithDelegate:andAppId:")]
		void InitWith (VKSdkDelegate sdkDelegate, string appId);

		[Static, Export ("initializeWithDelegate:andAppId:andCustomToken:")]
		void InitWith (VKSdkDelegate sdkDelegate, string appId, VKAccessToken token);

		[Static, Export ("authorize:")]
		void Authorize (NSArray permissions);

		[Static, Export ("authorize:revokeAccess:")]
		void Authorize (NSArray permissions, bool revokeAccess);

		[Static, Export ("authorize:revokeAccess:forceOAuth:")]
		void Authorize (NSArray permissions, bool revokeAccess, bool forceOAuth);

		[Static, Export ("authorize:revokeAccess:forceOAuth:inApp:")]
		void Authorize (NSArray permissions, bool revokeAccess, bool forceOAuth, bool inApp);

		[Static, Export ("authorize:revokeAccess:forceOAuth:inApp:display:")]
		void Authorize (NSArray permissions, bool revokeAccess, bool forceOAuth, bool inApp, string displayType);

		[Static, Export ("setAccessToken:")]
		void SetAccessToken (VKAccessToken token);

		[Static, Export ("setAccessTokenError:")]
		void SetAccessTokenError (VKError error);

		[Static, Export ("getAccessToken")]
		VKAccessToken GetAccessToken (VKError error);

		[Static, Export ("processOpenURL:fromApplication:")]
		bool ProcessOpenUrl (NSUrl passedUrl, string sourceApp);

		[Static, Export ("isLoggedIn")]
		bool IsLoggedIn { get; }

		[Static, Export ("wakeUpSession")]
		bool WakeUpSession { get; }

		[Static, Export ("forceLogout")]
		void ForceLogout ();
	}

	[BaseType (typeof(NSObject))]
	interface VKApi {
		[Static, Export ("users")]
		VKApiUsers Users { get; }

		[Static, Export ("wall")]
		VKApiWall Wall { get; }

		[Static, Export ("photos")]
		VKApiPhotos Photos { get; }

		[Static, Export ("friends")]
		VKApiFriends Friends { get; }

		[Static, Export ("requestWithMethod:andParameters:andHttpMethod:")]
		VKRequest Request (string method, NSDictionary parameters, string httpMethod);

		[Static, Export ("uploadWallPhotoRequest:parameters:userId:groupId:")]
		VKRequest UploadWallPhoto (UIImage image, VKImageParameters imageParameters, long userId, int groupId);
	}
	#endregion

	#region Controllers
	[BaseType (typeof(UIViewController))]
	interface VKAuthorizeController : IUIWebViewDelegate {
		[Static, Export("presentForAuthorizeWithAppId:andPermissions:revokeAccess:displayType:")]
		void PresentForAuthorizeWithAppId(string appId, NSArray permissions, bool revoke, string displayType);

		[Static, Export("presentForValidation:")]
		void PresentForValidation(VKError validationError);

		[Static, Export("buildAuthorizationUrl:clientId:scope:revoke:display:")]
		string BuildAuthorizationUrl(string redirectUri, string clientId, string scope, bool revoke, string displayType);
	}

	[BaseType (typeof(UIViewController))]
	interface VKCaptchaViewController {
		[Static, Export("captchaControllerWithError:")]
		VKCaptchaViewController CaptchaControllerWithError(VKError error);

		[Export("")]
		void PresentIn(UIViewController viewController);
	}
	#endregion
}

